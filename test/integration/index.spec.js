import CustomerBrandServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import jwt from 'jwt-simple';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be CustomerBrandServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(CustomerBrandServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listLevel1CustomerBrands method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig);

                /*
                 act
                 */
                const level1CustomerBrandsPromise =
                    objectUnderTest.listLevel1CustomerBrands(
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                level1CustomerBrandsPromise
                    .then((level1CustomerBrands) => {
                        expect(level1CustomerBrands.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            })
        });

        describe('listLevel2CustomerBrands method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig);

                /*
                 act
                 */
                const level2CustomerBrandsPromise =
                    objectUnderTest.listLevel2CustomerBrands(
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                level2CustomerBrandsPromise
                    .then((level2CustomerBrands) => {
                        expect(level2CustomerBrands.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            })
        });

    });
});