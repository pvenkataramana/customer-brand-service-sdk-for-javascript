import Level2CustomerBrandView from '../../src/level2CustomerBrandView';
import CustomerBrandSynopsisView from '../../src/customerBrandSynopsisView';
import dummy from '../dummy';

const validCustomerBrandSynopsisView = new CustomerBrandSynopsisView(1, 'name');

describe('Level2CustomerBrandView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level2CustomerBrandView(
                        null,
                        dummy.customerBrandName,
                        dummy.customerSegmentId,
                        validCustomerBrandSynopsisView
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new Level2CustomerBrandView(
                    expectedId,
                    dummy.customerBrandName,
                    dummy.customerSegmentId,
                    validCustomerBrandSynopsisView
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level2CustomerBrandView(
                        dummy.customerBrandId,
                        null,
                        dummy.customerSegmentId,
                        validCustomerBrandSynopsisView
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = 'valid name';

            /*
             act
             */
            const objectUnderTest =
                new Level2CustomerBrandView(
                    dummy.customerBrandId,
                    expectedName,
                    dummy.customerSegmentId,
                    validCustomerBrandSynopsisView
                );

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });


        it('throws if customerSegmentId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level2CustomerBrandView(
                        dummy.customerBrandId,
                        dummy.customerBrandName,
                        null,
                        validCustomerBrandSynopsisView
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'customerSegmentId required');
        });

        it('sets customerSegmentId', () => {
            /*
             arrange
             */
            const expectedCustomerSegmentId = 1;

            /*
             act
             */
            const objectUnderTest =
                new Level2CustomerBrandView(
                    dummy.customerBrandId,
                    dummy.customerBrandName,
                    expectedCustomerSegmentId,
                    validCustomerBrandSynopsisView
                );

            /*
             assert
             */
            const actualCustomerSegmentId = objectUnderTest.customerSegmentId;
            expect(actualCustomerSegmentId).toEqual(expectedCustomerSegmentId);

        });
        it('throws if level1CustomerBrand is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new Level2CustomerBrandView(
                        dummy.customerBrandId,
                        dummy.customerBrandName,
                        dummy.customerSegmentId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'level1CustomerBrand required');
        });
        it('sets level1CustomerBrand', () => {
            /*
             arrange
             */
            const expectedLevel1CustomerBrand = validCustomerBrandSynopsisView;

            /*
             act
             */
            const objectUnderTest =
                new Level2CustomerBrandView(
                    dummy.customerBrandId,
                    dummy.customerBrandName,
                    dummy.customerSegmentId,
                    validCustomerBrandSynopsisView
                );

            /*
             assert
             */
            const actualLevel1CustomerBrand = objectUnderTest.level1CustomerBrand;
            expect(actualLevel1CustomerBrand).toEqual(expectedLevel1CustomerBrand);

        });
    })
});
