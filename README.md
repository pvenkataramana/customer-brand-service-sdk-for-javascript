## Description
Precor Connect customer brand service SDK for javascript.

## Features

##### List Level 1 Customer Brands
* [documentation](features/ListLevel1CustomerBrands.feature)

##### List Level 2 Customer Brands
* [documentation](features/ListLevel2CustomerBrands.feature)

## Setup

**install via jspm**  
```shell
jspm install customer-brand-service-sdk=bitbucket:precorconnect/customer-brand-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import CustomerBrandServiceSdk,{CustomerBrandServiceSdkConfig} from 'customer-brand-service-sdk'

const customerBrandServiceSdkConfig = 
    new CustomerBrandServiceSdkConfig(
        "https://customer-brand-service-dev.precorconnect.com"
    );
    
const customerBrandServiceSdk = 
    new CustomerBrandServiceSdk(
        customerBrandServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.