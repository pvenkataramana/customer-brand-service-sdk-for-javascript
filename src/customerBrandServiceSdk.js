import CustomerBrandServiceSdkConfig from './customerBrandServiceSdkConfig';
import DiContainer from './diContainer';
import Level1CustomerBrandView from './level1CustomerBrandView';
import Level2CustomerBrandView from './level2CustomerBrandView';
import ListLevel1CustomerBrandsFeature from './listLevel1CustomerBrandsFeature';
import ListLevel2CustomerBrandsFeature from './listLevel2CustomerBrandsFeature';

/**
 * @class {CustomerBrandServiceSdk}
 */
export default class CustomerBrandServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {CustomerBrandServiceSdkConfig} config
     */
    constructor(config:CustomerBrandServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    listLevel1CustomerBrands(accessToken:string):Array<Level1CustomerBrandView> {

        return this
            ._diContainer
            .get(ListLevel1CustomerBrandsFeature)
            .execute(accessToken);

    }

    listLevel2CustomerBrands(accessToken:string):Array<Level2CustomerBrandView> {

        return this
            ._diContainer
            .get(ListLevel2CustomerBrandsFeature)
            .execute(accessToken);
    }

}
