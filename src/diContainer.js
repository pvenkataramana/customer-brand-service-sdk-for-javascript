import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import CustomerBrandServiceSdkConfig from './customerBrandServiceSdkConfig';
import ListLevel1CustomerBrandsFeature from './listLevel1CustomerBrandsFeature';
import ListLevel2CustomerBrandsFeature from './listLevel2CustomerBrandsFeature';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {CustomerBrandServiceSdkConfig} config
     */
    constructor(config:CustomerBrandServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(CustomerBrandServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(ListLevel1CustomerBrandsFeature);
        this._container.autoRegister(ListLevel2CustomerBrandsFeature);

    }

}
