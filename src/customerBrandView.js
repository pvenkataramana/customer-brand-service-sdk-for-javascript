/**
 * @class {CustomerBrandView}
 */
export default class CustomerBrandView {

    _id:number;

    _name:string;

    _customerSegmentId:number;

    /**
     * @param {number} id
     * @param {string} name
     * @param {number} customerSegmentId
     */
    constructor(id:number,
                name:string,
                customerSegmentId:number) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!customerSegmentId) {
            throw new TypeError('customerSegmentId required');
        }
        this._customerSegmentId = customerSegmentId;

    }

    /**
     * @returns {number}
     */
    get id():number {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get name():string {
        return this._name;
    }

    /**
     *
     * @returns {number}
     */
    get customerSegmentId():number{
        return this._customerSegmentId;
    }

}
