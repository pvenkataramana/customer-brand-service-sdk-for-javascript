import {inject} from 'aurelia-dependency-injection';
import CustomerBrandServiceSdkConfig from './customerBrandServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import Level2CustomerBrandView from './level2CustomerBrandView';
import Level2CustomerBrandViewFactory from './level2CustomerBrandViewFactory';

@inject(CustomerBrandServiceSdkConfig, HttpClient)
class ListLevel2CustomerBrandsFeature {

    _config:CustomerBrandServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedLevel2CustomerBrands:Array<Level2CustomerBrandView>;

    constructor(config:CustomerBrandServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all level2 customer brands
     * @param {string} accessToken
     * @returns a promise of {Level2CustomerBrandView[]}
     */
    execute(accessToken:string):Promise<Array> {

        if (this._cachedLevel2CustomerBrands) {

            return Promise.resolve(this._cachedLevel2CustomerBrands);

        }
        else {
            return this._httpClient
                .createRequest('level-2-customer-brands')
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                        // cache
                        this._cachedLevel2CustomerBrands =
                            Array.from(
                                response.content,
                                (contentItem) =>
                                    Level2CustomerBrandViewFactory.construct(contentItem)
                            );

                        return this._cachedLevel2CustomerBrands;
                    }
                );
        }
    }
}

export default ListLevel2CustomerBrandsFeature;
