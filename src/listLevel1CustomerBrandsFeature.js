import {inject} from 'aurelia-dependency-injection';
import CustomerBrandServiceSdkConfig from './customerBrandServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import Level1CustomerBrandView from './level1CustomerBrandView';

@inject(CustomerBrandServiceSdkConfig, HttpClient)
class ListLevel1CustomerBrandsFeature {

    _config:CustomerBrandServiceSdkConfig;

    _httpClient:HttpClient;

    _cachedLevel1CustomerBrands:Array<Level1CustomerBrandView>;

    constructor(config:CustomerBrandServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists all level1 customer brands
     * @param {string} accessToken
     * @returns a promise of {Level1CustomerBrandView[]}
     */
    execute(accessToken:string):Promise<Array> {

        if (this._cachedLevel1CustomerBrands) {

            return Promise.resolve(this._cachedLevel1CustomerBrands);

        }
        else {
            return this._httpClient
                .createRequest('level-1-customer-brands')
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(response => {

                        // cache
                        this._cachedLevel1CustomerBrands =
                            Array.from(
                                response.content,
                                (contentItem) =>
                                    new Level1CustomerBrandView(
                                        contentItem.id,
                                        contentItem.name,
                                        contentItem.customerSegmentId
                                    )
                            );

                        return this._cachedLevel1CustomerBrands;

                    }
                );
        }
    }
}

export default ListLevel1CustomerBrandsFeature;
