Feature: List Level 1 Customer Brands
  Lists all level 1 customer brands

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listLevel1CustomerBrands
    Then all level 1 customer brands in the customer-brand-service are returned